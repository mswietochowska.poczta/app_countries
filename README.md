# APP Cuntries Flask


# Project Name
> APP ROZWÓJ KRAJÓW WEDŁUG GNI


## General Information
Aplikacja powstała jako projekt końcowy bootcamp-u Python Developer 8 w Future Collars.
Zwraca informację o 3 krajach, które najbardziej zubożały ub wzbogaciły się 
w przedziale czasowym podanym przez użytkownika.


## Technologies Used
Python, Flask, Bootstrap, Numpy, Pandas

## Screenshots
![Example screenshot](./app_countries_screen.png)

## Usage
```bash
pip install -r requirements.txt
```


## Status projektu
Project is: _in progress.

## Jak udoskonalić aplikację

Dodać:
- Mapę z wyświetlanymi konturami krajów zwracanymi przez aplikację 1
- Raport z wynikami wyszukiwania do pobrania 2


## Acknowledgements
- Projekt inspirowany był przez Future Collars.
- Aplikacja używa danych z [tabeli]
  (https://databank.worldbank.org/reports.aspx?source=1296&series=NY.GNP.MKTP.PC.CD).






