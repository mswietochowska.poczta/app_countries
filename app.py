from flask import Flask, render_template
from flask_bootstrap import Bootstrap4
from wtforms import SelectField, SubmitField
from flask_wtf import FlaskForm
from filtering_data import DataProvider, load_data_to_dataframe

app = Flask(__name__)
app.config['SECRET_KEY'] = 'moj_klucz'
boot = Bootstrap4(app)

available_years = ['1990', '2000', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019']


class FilterFrom(FlaskForm):
    choose_type = SelectField(label="Zapytanie", choices=['Wzbogacenie - 3 kraje o najwyższej wartości wskażnika',
                                                          'Zubożenie - 3 kraje o najniższej wartości wskaźnika ',
                                                          "Spadek pozycji wg GNI",
                                                          "Wzrost pozycji wg GNI"])
    value_type = SelectField(label="Wartosc", choices=['Procentowo', 'Wartosciowo'])
    from_y = SelectField(label="Od", choices=available_years)
    to_y = SelectField(label="Do", choices=available_years)
    submit = SubmitField()


@app.route("/", methods=["GET", "POST"])
def index():
    form = FilterFrom()
    result = None
    if form.validate_on_submit():
        from_y = int(form.from_y.data)
        to_y = int(form.to_y.data)
        data = DataProvider(df=load_data_to_dataframe(), from_y=from_y, to_y=to_y)

        if form.choose_type.data == "Wzbogacenie - 3 kraje o najwyższej wartości wskażnika":
            if form.value_type.data == "Procentowo":
                result = data.get_countries_data_by_wealth_by_percentage()['most']
            elif form.value_type.data == "Wartosciowo":
                result = data.get_countries_data_by_wealth_by_value()['most']

        elif form.choose_type.data == "Zubożenie - 3 kraje o najniższej wartości wskaźnika ":
            if form.value_type.data == "Procentowo":
                result = data.get_countries_data_by_wealth_by_percentage()['least']
            elif form.value_type.data == "Wartosciowo":
                result = data.get_countries_data_by_wealth_by_value()['least']

        elif form.choose_type.data == "Spadek pozycji wg GNI":
            if form.value_type.data == "Wartosciowo":
                result = data.get_countries_position(reverse=False)

        elif form.choose_type.data == "Wzorst pozycji wg GNI":
            if form.value_type.data == "Wartosciowo":
                result = data.get_countries_position(reverse=True)

        return render_template("index.html", form=form, data=[result])

    return render_template("index.html", form=form, result=result)


if __name__ == '__main__':
    app.run()
