import pandas as pd

TABLE_CLASSES = "table table-striped"


def load_data_to_dataframe():
    data_frame = pd.read_csv("../../FutureCollars_Python/APP_Countries/dane_kraje.csv")
    return data_frame.iloc[46:].reset_index(drop=True)


class DataProvider:
    def __init__(self, df: pd.DataFrame, from_y, to_y):
        self._from_y = from_y
        self._to_y = to_y
        self.df = df
        self._convert_columns_to_numeric()

    @staticmethod
    def round_year(y, y1, y2):
        if abs(y - y2) < abs(y - y1):
            return y2
        else:
            return y1

    @property
    def from_year(self):
        if self._from_y < 1990:
            self._from_y = 1990
        elif 1990 < self._from_y < 2000:
            self._from_y = self.round_year(self._from_y, 1990, 2000)
        elif 2000 < self._from_y < 2010:
            self._from_y = self.round_year(self._from_y, 2000, 2010)

        elif self._from_y > 2019:
            self._from_y = 2019

        return f'{self._from_y} [YR{self._from_y}]'

    @property
    def to_year(self):
        if self._to_y < 1990:
            self._to_y = 1990
        elif 1990 < self._from_y < 2000:
            self._to_y = self.round_year(self._to_y, 1990, 2000)
        elif 2000 < self._to_y < 2010:
            self._to_y = self.round_year(self._to_y, 2000, 2010)
        elif self._to_y > 2019:
            self._to_y = 2019

        return f"{self._to_y} [YR{self._to_y}]"

    @property
    def related_rows_value(self):
        return ['Country Name', f"{self.from_year}", f"{self.to_year}", "GNI diff value"]

    @property
    def related_rows_percent(self):
        return ['Country Name', f"{self.from_year}", f"{self.to_year}", "%"]

    def _convert_columns_to_numeric(self):
        self.df[self.from_year] = pd.to_numeric(self.df[self.from_year], errors='coerce')
        self.df[self.to_year] = pd.to_numeric(self.df[self.to_year], errors='coerce')

    def calculate_diff(self):
        self.df['GNI diff value'] = (self.df[self.to_year] - self.df[self.from_year])

    def get_countries_data_by_wealth_by_value(self):
        self.calculate_diff()
        return {"most": self.df.nlargest(3, ['GNI diff value'])[self.related_rows_value].to_html(classes=TABLE_CLASSES,
                                                                                                 header=True,
                                                                                                 index=False),
                "least": self.df.nsmallest(3, ['GNI diff value'])[self.related_rows_value].to_html(
                    classes=TABLE_CLASSES,
                    header=True, index=False)}

    def get_countries_data_by_wealth_by_percentage(self):
        self.calculate_diff()
        self.df['%'] = self.df['GNI diff value'] * 100 / self.df[self.to_year]
        return {"least": self.df.nsmallest(3, ['%'])[self.related_rows_percent].to_html(classes=TABLE_CLASSES,
                                                                                        header=True, index=False),
                "most": self.df.nlargest(3, ["%"])[self.related_rows_percent].to_html(classes=TABLE_CLASSES,
                                                                                      header=True, index=False)}

    def get_countries_position(self, reverse: bool = True):
        df_from = self.df[["Country Name", f"{self.from_year}"]].sort_values(by=[self.from_year], ignore_index=True,
                                                                             ascending=False).dropna()

        df_to = self.df[["Country Name", f"{self.to_year}"]].sort_values(by=[self.to_year],
                                                                         ignore_index=True, ascending=False).dropna()
        result = {}
        for i, elem_from in enumerate(df_from[["Country Name", self.from_year]].values, 1):
            for j, elem_to in enumerate(df_to[["Country Name", f"{self.to_year}"]].values, 1):
                if elem_from[0] == elem_to[0]:
                    result[elem_from[0]] = {"before": i}
                    result[elem_to[0]]["after"] = j
                    result[elem_to[0]]["diff"] = result[elem_to[0]]["before"] - result[elem_to[0]]["after"]
                    break

        res = sorted(result.items(), key=lambda item: item[1]['diff'], reverse=reverse)
        df = pd.DataFrame.from_dict(dict(res[:3]), orient='index')
        df['Country Name'] = df.index
        new_cols = ["Country Name", "before", "after", "diff"]
        df = df.reindex(columns=new_cols)
        return df.to_html(classes=TABLE_CLASSES, header=True, index=False)


def get_data():
    df = load_data_to_dataframe()
    d_p = DataProvider(df, 1990, 2010)
    d_p.get_countries_position()


if __name__ == '__main__':
    get_data()
